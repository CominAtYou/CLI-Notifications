﻿using Microsoft.Toolkit.Uwp.Notifications;

namespace Program {
    class Program {
        static void Main(string[] args) {
            if (args.Length == 0) {
                Help.display();
                Environment.Exit(0);
            }

            if (args.Contains("-ToastActivated")) Environment.Exit(0);
            Arguments.runUtilityArguments(args);
            Arguments.verify(args);

            bool wantsConfirmation = args.Contains("--confirmation");
            bool wantsClickableWithoutConfirmation = !wantsConfirmation && args.Contains("--clickable");

            ToastContentBuilder notification = Arguments.apply(new ToastContentBuilder(), args);

            ToastNotificationManagerCompat.OnActivated += Notifications.onActivated(wantsConfirmation);

            notification.Show();

            if (wantsConfirmation || wantsClickableWithoutConfirmation) while(true) { continue; } // Keep the app from closing until notification is interacted with
        }
    }
}
