using Microsoft.Toolkit.Uwp.Notifications;

namespace Program {
    namespace Args {
        class Confirmation {
            public static ToastContentBuilder addButtons(ToastContentBuilder builder, string[] args) {
                int positiveIndex = Array.IndexOf(args, "--positivetext");
                int negativeIndex = Array.IndexOf(args, "--negativetext");

                return builder.AddButton(new ToastButton()
                    .SetContent(positiveIndex != -1 ? Notifications.createTextElement(args, "--positivetext") : "Yes")
                    .AddArgument("action", "positiveSelection")
                )
                .AddButton(new ToastButton()
                    .SetContent(negativeIndex != -1 ? Notifications.createTextElement(args, "--negativetext") : "No")
                    .AddArgument("action", "negativeSelection")
                );
            }
        }
    }
}
