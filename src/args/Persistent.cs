using Microsoft.Toolkit.Uwp.Notifications;

namespace Program {
    namespace Args {
        class Persistent {
            public static ToastContentBuilder setPersistent(ToastContentBuilder builder, string[] args) {
                return builder.SetToastScenario(ToastScenario.Reminder);
            }
        }
    }
}

