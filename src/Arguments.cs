using Microsoft.Toolkit.Uwp.Notifications;
using Program.Args;

namespace Program {
    class Arguments {
        private static List<string> arguments = new List<string>() { "--title", "--description", "--confirmation", "--positivetext", "--negativetext", "--clickable", "--persistent", "--avatar", "--image" };
        private static Dictionary<string, Func<ToastContentBuilder, string[], ToastContentBuilder>> argOps = new Dictionary<string, Func<ToastContentBuilder, string[], ToastContentBuilder>>() {
            { "--title", Title.addTitle },
            { "--description", Description.addDescription },
            { "--confirmation", Confirmation.addButtons },
            { "--persistent", Persistent.setPersistent },
            { "--avatar", Avatar.setAvatar },
            { "--image", Image.setImage }
        };

        public static void verify(string[] args) {
            // Ensure the command includes a title.
            if (!args.Contains("--title")) {
                int argIndex = Array.IndexOf(args, "--title");

                if (argIndex == args.Length - 1) {
                    Console.Error.WriteLine("Required arguments are either missing or invalid!");
                    Environment.Exit(3);
                }

                // check if title actually has a value by checking if the element after --title starts with a --.
                if (args[argIndex + 1].StartsWith("--")) {
                    Console.Error.WriteLine("Invalid value provided for title parameter!");
                    Environment.Exit(3);
                }
            }

            if (args.Contains("--description")) {
                int argIndex = Array.IndexOf(args, "--description");

                if (argIndex == args.Length - 1) {
                    Console.Error.WriteLine("Invalid value provided for description parameter!");
                    Environment.Exit(3);
                }

                // check if description actually has a value by checking if the element after --description starts with a --.
                if (args[argIndex + 1].StartsWith("--")) {
                    Console.Error.WriteLine("Invalid value provided for description parameter!");
                    Environment.Exit(3);
                }
            }

            if (args.Contains("--positivetext")) {
                int argIndex = Array.IndexOf(args, "--positivetext");

                if (argIndex == args.Length - 1) {
                    Console.Error.WriteLine("Invalid value provided for positivetext parameter!");
                    Environment.Exit(3);
                }

                // check if positivetext actually has a value by checking if the element after --positivetext starts with a --.
                if (args[argIndex + 1].StartsWith("--")) {
                    Console.Error.WriteLine("Invalid value provided for positivetext parameter!");
                    Environment.Exit(3);
                }
            }

            // same as title, check if negativetext actually has a value by checking if the element after --negativetext starts with a --.
            if (args.Contains("--negativetext")) {
                int argIndex = Array.IndexOf(args, "--negativetext");

                if (argIndex == args.Length - 1) {
                    Console.Error.WriteLine("Invalid value provided for negativetext parameter!");
                    Environment.Exit(3);
                }

                if (args[argIndex + 1].StartsWith("--")) {
                    Console.Error.WriteLine("Invalid value provided for negativetext parameter!");
                    Environment.Exit(3);
                }
            }

            for (int i = 0; i < args.Length; i++) {
                if (args[i].StartsWith("--") && !arguments.Contains(args[i])) {
                    Console.Error.WriteLine("Unknown parameter '{0}'", args[i]);
                    Environment.Exit(3);
                }
            }
        }

        public static ToastContentBuilder apply(ToastContentBuilder builder, string[] args) {
            ToastContentBuilder builderInstance = builder;

            for (int i = 0; i < args.Length; i++) {
                if (!args[i].StartsWith("--")) continue;
                if (argOps.ContainsKey(args[i])) {
                    builderInstance = argOps[args[i]](builderInstance, args);
                }
            }

            return builderInstance;
        }

        public static void runUtilityArguments(string[] args) {
            if (args.Contains("--help")) Help.display();

            if (args[0] == "--uninstall") {
                ToastNotificationManagerCompat.Uninstall();
                Console.WriteLine("Cleaned up notifications and notification resources.");
                Environment.Exit(0);
            }

            if (args.Length == 0) {
                Console.Error.WriteLine("No arguments provided!");
                Environment.Exit(3);
            }
        }
    }
}
